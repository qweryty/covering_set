#!/bin/bash

IFS_bak=$IFS
IFS=$'\n'
for i in `./permutations $@ | sort -u`;
do
	echo $i
	echo $i | xargs ./jenny | wc -l
done
IFS=$IFS_bak
